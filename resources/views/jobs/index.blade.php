@extends('layouts.app')
@section('title', 'Вакансии')
@section('content')
  <!-- JOBS -->
  <div class="jobs-block wow bounceInRight">
    <div class="container">
        <h1 class="display-4">Вакансии</h1>
        <div class="row">
            @foreach ($jobs as $job)
            <div class="col-12 col-sm-12 col-md-4">
                <div class="jobs-1">
                    <h6>{{ $job->title }}</h6>
                    <p><span class="text-style">{{ $job->salary }}тг</span> / {{ $job->payment_type }} <br>{{ $job->employment_type }}</p>
                </div>
            </div>
            @endforeach
        </div>
        {{ $jobs->links() }}
    </div>	
</div>
<!-- JOBS -->  
@endsection