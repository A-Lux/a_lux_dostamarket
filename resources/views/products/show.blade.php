@extends('layouts.app')
@section('title', $product->title)
@section('content')
<div class="catalog">
    <div class="container">
        <span class="catalog-link"><a href="{{ route('catalog.page', 1) }}">В каталог</a></span>
        <div class="catalog-img-text">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-5">
                    <div class="catalog-img text-center">
                        <img src="{{ asset('storage/'.$product->image) }}" alt="{{ $product->title }}">
                    </div>
                </div>
            <div class="col-12 col-sm-12 col-md-7">
                <div class="catalog-menu">
                    <h3>{{ $product->title }}</h3>
                    <p>{{ $product->short_description }}</p>
                    <br>
                    <span class="catalog-price"><h4 class="font-weight-bold">{{ $product->price }} тг</h4></span>
                    <br>
                    <div class="number">
                    <div class="btn btn-danger minus">-</div>
                    <input type="text" value="1" id="item-counter"/>
                    <div class="btn btn-success plus">+</div>
                    </div>
                    <br>
                    <div class="btn-card">
                        <button class="btn btn-success btn-korzina" id="add-to-cart" prodid="{{ $product->id }}"><img src="{{ asset('img/korzina.png') }}">В корзину</button>
                        <button class="btn btn-like btn-danger btn-korzina add-to-fav" prod="{{ $product->id }}"><img src="{{ asset('img/like-catalog.png') }}">Изранное</button>
                    </div>
                    </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12">
                <div class="catalog-text">
                <p>
                    {!! $product->description !!}
                </p>
            </div>
            </div>
        </div>
    </div>
</div>
<!-- KATALOG -->
<!-- МОДАЛ КОНТЕНТ -->
<div class="modal fade" id="modal-basket" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5>Товар добалвен в корзину</h5>
        </button>
        </div>
        <div class="modal-btn">
            <button type="button" class="btn btn-light btn-modal-1" data-dismiss="modal">Продолжить покупки</button>
            <button type="button" class="btn btn-success btn-modal-2" data-dismiss="modal">Оформить заказ</button>
        </div>
    </div>
    </div>
</div>

<!-- МОДАЛ КОНТЕНТ -->

<!-- CATALOG-CAROUSEL -->
<div class="carousel-card">
<div class="container">
    <h4>ЧАСТО ПОКУПАЮТ</h4>
    <div class="row">
        <div class="col-md-12">
            <div class="carousel-block">
            <div class="prev_card">
            <a href="#" class="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
            </div>
            <div class="carousel-js">
                <div class="owl-carousel owl-theme catalog-slider-bravolli">
                    <div class="text-center">
                        <div class="carousel-card-img">
                        <div class="carosel-card text-center">
                            <a href="#"><img src="img/like.png" alt="" class="like"></a>
                            <div class="card-body">
                                <img src="img/card.png" alt="...">
                        <p class="card-text text-left">Кукурузная крупа "Ярамарка Отборная" 4 пакета для варки по 62,5г <br><span>340 тг</span></p>
                            </div>
                            <div class="card-text-2">
                                <button class="btn btn-success btn-korzina"  data-toggle="modal" data-target="#modal-basket"><img src="img/korzina.png" alt="">В корзину</button>
                                <br>
                                <a href="#">Подробнее</a>
                            </div>
                        </div>
                        </div>
                    </div>
                        <div class="text-center">
                        <div class="carousel-card-img">	
                        <div class="carosel-card text-center">
                            <a href="#"><img src="img/like.png" alt="" class="like"></a>
                            <div class="card-body">
                                <img src="img/card.png" alt="...">
                        <p class="card-text text-left">Кукурузная крупа "Ярамарка Отборная" 4 пакета для варки по 62,5г <br><span>340 тг</span></p>
                            </div>
                            <div class="card-text-2">
                                <button class="btn btn-success btn-korzina"  data-toggle="modal" data-target="#modal-basket"><img src="img/korzina.png" alt="">В корзину</button>
                                <br>
                                <a href="#">Подробнее</a>
                            </div>
                        </div>
                        </div>
                    </div>		
                        <div class="text-center">
                        <div class="carousel-card-img">
                        <div class="carosel-card text-center">
                            <a href="#"><img src="img/like.png" alt="" class="like"></a>
                            <div class="card-body">
                                <img src="img/card.png" alt="...">
                        <p class="card-text text-left">Кукурузная крупа "Ярамарка Отборная" 4 пакета для варки по 62,5г <br><span>340 тг</span></p>
                            </div>
                            <div class="card-text-2">
                                <button class="btn btn-success btn-korzina"  data-toggle="modal" data-target="#modal-basket"><img src="img/korzina.png" alt="">В корзину</button>
                                <br>
                                <a href="#">Подробнее</a>
                            </div>
                        </div>
                        </div>
                    </div>	
                        <div class="text-center">
                        <div class="carousel-card-img">
                        <div class="carosel-card text-center">
                            <a href="#"><img src="img/like.png" alt="" class="like"></a>
                            <div class="card-body">
                                <img src="img/card.png" alt="...">
                        <p class="card-text text-left">Кукурузная крупа "Ярамарка Отборная" 4 пакета для варки по 62,5г <br><span>340 тг</span></p>
                            </div>
                            <div class="card-text-2">
                                <button class="btn btn-success btn-korzina" data-toggle="modal" data-target="#modal-basket"><img src="img/korzina.png" alt="">В корзину</button>
                                <br>
                                <a href="#">Подробнее</a>
                            </div>
                        </div>
                        </div>
                    </div>	
                        <div class="text-center">
                        <div class="carousel-card-img">
                        <div class="carosel-card text-center">
                            <a href="#"><img src="img/like.png" alt="" class="like"></a>
                            <div class="card-body">
                                <img src="img/card.png" alt="...">
                        <p class="card-text text-left">Кукурузная крупа "Ярамарка Отборная" 4 пакета для варки по 62,5г <br><span>340 тг</span></p>
                            </div>
                            <div class="card-text-2">
                                <button class="btn btn-success btn-korzina"  data-toggle="modal" data-target="#modal-basket"><img src="img/korzina.png" alt="">В корзину</button>
                                <br>
                                <a href="#">Подробнее</a>
                            </div>
                        </div>
                        </div>
                    </div>	
                        <div class="text-center">
                        <div class="carousel-card-img">
                        <div class="carosel-card text-center">
                            <a href="#"><img src="img/like.png" alt="" class="like"></a>
                            <div class="card-body">
                                <img src="img/card.png" alt="...">
                        <p class="card-text text-left">Кукурузная крупа "Ярамарка Отборная" 4 пакета для варки по 62,5г <br><span>340 тг</span></p>
                            </div>
                            <div class="card-text-2">
                                <button class="btn btn-success btn-korzina"  data-toggle="modal" data-target="#modal-basket"><img src="img/korzina.png" alt="">В корзину</button>
                                <br>
                                <a href="#">Подробнее</a>
                            </div>
                        </div>
                        </div>
                    </div>	
                        <div class="text-center">
                        <div class="carousel-card-img">
                        <div class="carosel-card text-center">
                            <a href="#"><img src="img/like.png" alt="" class="like"></a>
                            <div class="card-body">
                                <img src="img/card.png" alt="...">
                        <p class="card-text text-left">Кукурузная крупа "Ярамарка Отборная" 4 пакета для варки по 62,5г <br><span>340 тг</span></p>
                            </div>
                            <div class="card-text-2">
                                <button class="btn btn-success btn-korzina"  data-toggle="modal" data-target="#modal-basket"><img src="img/korzina.png" alt="">В корзину</button>
                                <br>
                                <a href="#">Подробнее</a>
                            </div>
                        </div>
                        </div>
                    </div>	
                        <div class="text-center">
                        <div class="carousel-card-img">
                        <div class="carosel-card text-center">
                            <a href="#"><img src="img/like.png" alt="" class="like"></a>
                            <div class="card-body">
                                <img src="img/card.png" alt="...">
                        <p class="card-text text-left">Кукурузная крупа "Ярамарка Отборная" 4 пакета для варки по 62,5г <br><span>340 тг</span></p>
                            </div>
                            <div class="card-text-2">
                                <button class="btn btn-success btn-korzina"  data-toggle="modal" data-target="#modal-basket"><img src="img/korzina.png" alt="">В корзину</button>
                                <br>
                                <a href="#">Подробнее</a>
                            </div>
                        </div>
                        </div>
                    </div>	
                </div>
            </div>
        <div class="next_card">
        <a href="#" class="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
        </div>
            </div>
        </div>
        </div>
</div>
</div>
        
@endsection