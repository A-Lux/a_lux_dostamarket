@extends('layouts.app')
@section('title', 'Контакты')
@section('content')
<!-- CONTACTS  -->
<div class="contacts-block wow bounceInRight">
    <div class="container">
        <div class="col-11 col-sm-11 col-md-11 offset-md-1">
            <h1>Контакты</h1>
        </div>
        <div class="row">
            <div class="col-12 col-sm-12 col-md-5 offset-md-1">
            <div class="contacts">
            <div class="contacts-loc">
                <p>Бизнес-Центр "KDS", проспект <br>Достык 38,7этаж 7, офис 705 <br>Алматы,Республика Казахстан</p>
            </div>
            <div class="location">
                <p>+7 727 357 20 00</p>
                <p>+7 771 357 20 00</p>
            </div>
            <div class="email">
                <i class="fa fa-envelope" aria-hidden="true"></i>
                <a href="#"> info@dostamarket.kz</a>
            </div>
            <br>
            <div class="email">
                <img src="img/clock.png" alt="" style="display: inline-block;">
                <p style="display: inline-block;">ежедневно с 9-00 до  18-00</p> 
            </div>
            <br>
            <div class="col-12 col-sm-12 col-md-6 ">
                    <button class="btn btn-contact">Обратная связь</button>
            </div>		
            </div>
            </div>
            @php $location = explode(',', $contacts->location) @endphp
            <div class="col-12 col-sm-12 col-md-6">
                <div class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23247.82206276441!2d{{ $location[1] }}!3d{{ $location[0] }}!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3883695a837c3227%3A0x3c4f1c527f7fad67!2z0JDQu9C80LDQu9C40L3RgdC60LjQuSDRgNCw0LnQvtC9LCDQkNC70LzQsNGC0Ys!5e0!3m2!1sru!2skz!4v1562221582515!5m2!1sru!2skz" width="600" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    </div>
<!-- CONTACTS  -->
@endsection