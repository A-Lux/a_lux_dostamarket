@extends('layouts.app')
@section('content')
<script type="text/javascript">
	var products = [];
        @foreach($products as $product)
            products.push({
                name: "{{ $product['title'] }}",
                price: {{ $product['price'] }},
                amount: {{ $product['amount'] }},
                id: {{ $product['id'] }}
            });
        @endforeach
        let token = document.head.querySelector('meta[name="csrf-token"]');
        var totalSum = 0;
        
        function refreshTotalSum(){
            totalSum = 0;
            for (i=0; i<products.length; i++){
            	$('#result'+products[i].id).text(products[i].price*products[i].amount);
            	$('#resultMobile'+products[i].id).text(products[i].price*products[i].amount);
                totalSum=totalSum+products[i].price*products[i].amount;
            }
            document.getElementById('FinalResult').innerHTML=totalSum; 
            document.getElementById('FinalResultMobile').innerHTML=totalSum; 
        }        

        function deleteProduct(id) {   
            var index = getIndexByValue(products, id);
            products[index].amount = 0;
            refreshTotalSum();
            axios.post("{{ route('cart.remove') }}", {id: id, "_token": "{{ csrf_token() }}"})
                	.then(r => console.log('test'));
            $('#productString'+id).remove()
            $('#rowMobile'+id).remove()
        }
        function eraseProducts() {
			for(i=0; i<products.length; i++){
				for (i=0; i<products.length; i++){
					products[i].amount=0;
					$('#productString'+products[i].id).remove()
					$('#rowMobile'+products[i].id).remove()
				}				
			}
			axios.post("{{ route('cart.clear') }}", {"_token": "{{ csrf_token() }}"})
                	.then(r => refreshTotalSum());
						
		}
        function getIndexByValue(arr, value) {
          for (var i=0, iLen=arr.length; i<iLen; i++) {
            if (arr[i].id == value) return i;
          }
        }
        function plusAmount(id){
            var obj = document.getElementById('inputAmount'+id)
            obj.stepUp()
            document.getElementById('amountMobile'+id).stepUp();
            var index = getIndexByValue(products, id);
            document.getElementById('result'+id).innerHTML = products[index].price*obj.value;
            document.getElementById('resultMobile'+id).innerHTML = products[index].price*obj.value;
            products[index].amount = products[index].amount + 1;
            refreshTotalSum();
            axios.post("{{ route('cart.add') }}", {id: id, q: products[index].amount, "_token": "{{ csrf_token() }}"});
        }
        
        function minusAmount(id){
            var obj = document.getElementById('inputAmount'+id)
            objm = document.getElementById('amountMobile'+id).stepUp();
            if(obj.value>1 || objm>1){
                obj.stepDown()
                document.getElementById('amountMobile'+id).stepDown();
                var index = getIndexByValue(products, id);
                document.getElementById('result'+id).innerHTML = products[index].price*obj.value + 'тг';
                document.getElementById('resultMobile'+id).innerHTML = products[index].price*obj.value;
                products[index].amount = products[index].amount - 1;
                refreshTotalSum();
                // Axios.post( "{{ route('cart.add') }}?q="+products[index].amount+"&id="+id, function( data ) {});
                axios.post("{{ route('cart.add') }}", {id: id, q: products[index].amount, "_token": "{{ csrf_token() }}"});
            }
        }

        function sendOrder(){
            $.ajaxSetup({
            headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
            });
            $.post("/cart/order", function( data ) {
                if(data == "1"){
                    Swal.fire(
                      'Ваш заказ принят!',
                      'Наш менеджер скоро свяжется с вами',
                      'Принято'
                    )
                }
                else{
                    if(data == "2"){
                        $('#signin').modal('show');
                    }
                    else{
                        Swal.fire(
                          'Что то пошло не так',
                          'Произошла какая то проблема',
                          'Закрыть'
                        )
                    }
                }
            }); 
        }
</script>
		<div class="basket-block">
			<div class="container">
			<div class="basket-title">
				<h1>КОРЗИНА ТОВАРОВ</h1>
			</div>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-3">
					<p>Наименование</p>
				</div>
				<div class="col-12 col-sm-12 col-md-3">
					<p>Цена</p>
				</div>
				<div class="col-12 col-sm-12 col-md-3">
					<p>Количество</p>
				</div>
				<div class="col-12 col-sm-12 col-md-3">
					<p>Всего</p>
				</div>
			</div>
			<hr>

			<hr>
			@foreach($products as $product)
			<div class="row" id="productString{{ $product['id'] }}">
				<div class="col-12 col-sm-12 col-md-3">
					<p>{{ $product['title'] }}</p>
				</div>
				<div class="col-12 col-sm-12 col-md-3">
					<div class="number"><p>{{ $product['price'] }} тг</p></div>
				</div>
				<div class="col-12 col-sm-12 col-md-3">
					<div class="number">
					<span class="minus" onclick="minusAmount({{ $product['id'] }})">-</span>
					<input disabled type="number" value="{{ $product['amount'] }}" id="inputAmount{{ $product['id'] }}">
					<span class="plus" onclick="plusAmount({{ $product['id'] }})">+</span>
				</div>
				</div>
				<div class="col-12 col-sm-12 col-md-2 number"><p><span id="result{{ $product['id'] }}">0</span> тг</p></div>
				<div class="col-12 col-sm-12 col-md-1 text-center">
					<div class="number" onclick="deleteProduct({{ $product['id'] }})"><img src="img/delete.png" alt=""></div>
				</div>
			</div>
			<hr>
			@endforeach
			<DIV class="row">
				<div class="col-12 col-sm-12 col-md-8 text-left">
					<div onclick="eraseProducts()"><img src="img/delete.png" alt=""><span class="basket-p" >Очистить корзину</span></div>
				</div>
				<div class="col-12 col-sm-12 col-md-4 text-right" >
					<div class="busket-button">
						<p>Всего к оплате:</p>
					<button class="btn btn-danger" id="FinalResult">0</button>
					</div>
					<form action="{{ route('order') }}" method="post">
						@csrf
						<button @if(\Auth::check())type="submit" @else onclick="$('#exampleModalCenter').modal('show')" @endif class="btn btn-success">Оформить заказ</button>	
					</form>
				</div>
			</DIV>
		</div>
		</div>
	
	<!-- BASKET-BLOCK -->

	<!-- BASKET-BLOCK-MOBILE -->
	<div class="basket-mobile">
		<div class="container">
			<div class="basket-title">
				<h1>КОРЗИНА ТОВАРОВ</h1>
			</div>
			<div class="basket-border">
				<div class="col-12">
					<div class="basket-text">
						@foreach($products as $product)
							<div class="basket-mobile-card" id="rowMobile{{ $product['amount'] }}">
								<p>Наименование</p>
								<p>{{ $product['title'] }}</p>
								<p>Цена</p>
								<div class="number"><p>{{ $product['price'] }}тг</p></div>
								<p>Количество</p>
								<div class="number text-center">
									<span class="minus" onclick="minusAmount({{$product['id']}})">-</span>
									<input type="number" disabled id="amountMobile{{$product['id']}}" value="{{ $product['amount'] }}"/>
									<span class="plus" onclick="plusAmount({{$product['id']}})">+</span>
								</div>
								<p>Всего</p>
								<div class="number"><p><span id="resultMobile{{ $product['id'] }}">0</span>тг</p></div>
							</div>
						@endforeach
					</div>
			
					</div>
			</div>
			
			<div class="basket-button-mobile">
				<div class="col-12 col-sm-12 col-md-8 text-left">
					<div onclick="eraseProducts()"><img src="img/delete.png" alt=""><span class="basket-p">Очистить корзину</span></div>
				</div>
				<div class="col-12 col-sm-12 col-md-4 text-right" >
					<div class="busket-button">
						<p>Всего к оплате:</p>
						<button class="btn btn-danger" id="FinalResultMobile">0</button>
					</div>
					<button class="btn btn-success" onclick="sendOrder()">Оформить заказ</button>
				</div>
			</DIV>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function (){
            refreshTotalSum();
        });
	</script>
@endsection
