@extends('layouts.app')
@section('title', 'Поиск')
@section('content')
<!-- Bakaleya-BLOCK -->
<div class="container">
    <div class="bakaleya-title">
        <h1 class="display-4">Поиск - {{ \Request::input('query') }}</h1>
    </div>
    <div class="bakaleya-block">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12">
                <div id="tab-krupa-1" class=" krupa-active tab-sort">
                    <div class="row">
                        @if($products->count() > 0)
                        @foreach ($products as $product)
                        <div class="col-12 col-sm-12 col-md-4">
                            <div class="card-bg wow fadeInUp">
                                <div class="text-right">
                                    <form action="" id="like" >
                                        <input type="hidden" name="like" value="0">
                                        <a href="#like" class="like-btn"><i id="like-button" class="fa fa-2x fa-heart-o not-liked"></i></a>
                                    </form>
                                </div>
                                <div class="card_ text-center">
                                    <img src="{{ asset('storage/'.$product->image) }}" alt="{{ $product->title }}">
                                    <div class="card-body">
                                        <p class="card-text text-left">{{ $product->title }}  {{ $product->short_description }}<br><span>{{ $product->price }} тг</span></p>
                                    </div>
                                    <div class="card-text-2">
                                        <button class="btn btn-success btn-korzina" data-toggle="modal" data-target="#modal-basket"><img src="img/korzina.png" alt="">В корзину</button>
                                        <br>
                                        <a href="{{ route('product.page', $product->id) }}">Подробнее</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @else
                        <h5>По запросу {{ \Request::input('query') }} ничего не найдено</h5>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection