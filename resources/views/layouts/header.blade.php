<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Market</title>
	<link rel="stylesheet" href="css/aos.css">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" href="css\font-awesome.css">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>


	<!-- header -->

	<header>
		<div class="container">
			<div class="header wow fadeInDown">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-3">
						<div class="header-logo">
							<a href="index.html"><img src="img/logo.png" alt=""></a>
						</div>
					</div>
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-9">
						<div class="hide-main">
							<div class="row">
							<div class="col-6 col-sm-6 col-md-6 col-lg-3 col-xl-4">
								<div class="job-clock">
									<img src="img/clock.png">
									<h6>График работы</h6>
								</div>
								<div style="display: inline-block;" class="job-clock_1">
									<p>с 8:00 до 24:00</p>
								</div>
							</div>
							<div class="col-6 col-sm-6 col-md-6 col-lg-3 col-xl-2">
								<div class="job-clock">
									<img src="img/location.png">
									<h6>Ваш город</h6>
								</div>
								<div style="display: inline-block;" class="job-clock_2">
									<p>Алматы</p>
								</div>
							</div>
							<div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-2">
								<div class="user">
									<a href="#" data-toggle="modal" data-target="#exampleModalCenter">Войти <span>/</span></a>
									<a href="#" data-toggle="modal" data-target="#exampleModalCenter-login">Зарегистрироваться</a>
								</div>
							</div>
							<div class="col-1 col-sm-1 col-md-2 col-lg-3 col-xl-1 basket">
								<a href="basket.html"><img src="img/basket.png"></a>
							</div>
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-3 ">
								<button class="btn btn-danger btn-header" data-toggle="modal" data-target="#exampleModalCenter-feedback"><i><img src="img/phone-header.png" ></i>Обратная связь</button>
							</div>
						</div>
						</div>
						<div class="main-form">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">
									<div class="main">
										<img src="img/menu.png" alt="">
										<h6>Menu</h6>
									</div>
								</div>
								<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
									<form action="form" class="header-form">
										<input type="text" placeholder="Введите название продукта">
										<button class="btn btn-success btn-form"><i class="fa fa-search" aria-hidden="true"></i>Найти</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<div>
		<div class="submenu" id="submenu">
			<div class="container">
				<div><img src="img/burger-exit.png" id="exit" alt=""></div>
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2"><a href="company.html">О компании</a></div>
					<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2"><a href="catalog.html">Каталог</a></div>	
					<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2"><a href="delivery.html">Доставка и оплата</a></div>
					<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2"><a href="jobs.html">Вакансии</a></div>
					<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2"><a href="#">Вопросы и ответы</a></div>
					<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2"><a href="contacts.html">Контакты</a></div>
				</div>
				<div class="location-mob " style="display: none;">
					<div class="row">
						<div class="col-12">
							<div class="job-clock-mob text-center">
								<img src="img/clock.png">
								<h6>График работы</h6>
							</div>
							<div style="display: inline-block;" class="job-clock_mob-1">
								<p>с 8:00 до 24:00</p>
							</div>
						</div>
						<div class=" col-12">
							<div class="job-clock_mob text-center">
								<img src="img/location.png">
								<h6>Ваш город</h6>
							</div>
							<div style="display: inline-block;" class="job-clock_mob-1">
								<p>Алматы</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="user-mobile">
			<button class="btn btn-danger btn-feedback-mobile" data-toggle="modal" data-target="#exampleModalCenter-feedback"><i><img src="img/phone-header.png" ></i></button>
			<div>
				<a href="#" data-toggle="modal" data-target="#exampleModalCenter"  class="user-mob"><i class="far fa-user-circle"></i></a>
				<a href="basket.html" class="basket-mob"><img src="img/basket.png"></a>
			</div>
		</div>
	</div>

	<!-- МОДАЛЬНОЕ ОКНО -ВОЙТИ -->
	<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Вход</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="modal-text">
						<p>С помощью аккаунта</p>
						<strong><a href="#">Создать аккаунт</a></strong>	
					</div>
					<input type="email" placeholder="Ваш E-mail">
					<input type="password" placeholder="Пароль">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" data-dismiss="modal">Войти</button>
					<a href="#">Забыли пароль?</a>
				</div>
			</div>
		</div>
	</div>

	<!-- МОДАЛЬНОЕ ОКНО -ОБРАТНАЯ СВЯЗЬ -->
	<div class="modal fade" id="exampleModalCenter-feedback" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Обратный звонок</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="">
						<input type="email" placeholder="Ваш E-mail">
						<input type="text" placeholder="Ваше имя">
						<div class="modal-footer">
							<button type="button" class="btn btn-success" data-dismiss="modal">Отправить</button>
						</form>
					</div>
				</div>
				<div class="form-group form-check">
					<form action="">
						<input type="checkbox" class="form-check-input" id="exampleCheck1">
						<label class="form-check-label" for="exampleCheck1">Я соглашаюсь с Политикой конфиденциальности обработки персональных данных и даю согласие на обработку своих персональных данных.</label>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- МОДАЛЬНОЕ ОКНО ЗАРЕГЕСТРИРОВАТЬСЯ -->
	<div class="modal fade" id="exampleModalCenter-login" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Регистрация</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<input type="email" placeholder="Ваш E-mail">
					<input type="password" placeholder="Придумайте пароль">
					<p>[Не короче 6 символов]</p>
					<input type="password" placeholder="Повторите пароль">
					<input type="password" placeholder="Ваше имя">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" data-dismiss="modal">Зарегистрироваться</button>
				</div>
			</div>
		</div>
	</div>

	<!-- header -->