
	<!-- МОДАЛ КОНТЕНТ -->
	<div class="modal fade" id="modal-basket" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5>Товар добавлен в корзину</h5>
				</button>
			</div>
			<div class="modal-btn">
				<button type="button" class="btn btn-light btn-modal-1" data-dismiss="modal">Продолжить покупки</button>
				<a type="button" class="btn btn-success btn-modal-2" href="/cart">Оформить заказ</a>
			</div>
		</div>
	</div>
</div>

<!-- МОДАЛ КОНТЕНТ -->

<!-- СКРОЛЛ ВВЕРХ -->
<div>
	<img src="img/Arrows-up-icon.png" alt="" class="scrollup">
</div>
<!-- СКРОЛЛ ВВЕРХ -->

<!-- Bakaleya-BLOCK -->

<!--FOOTER  -->
<footer class="container-fluid wow bounceInRight">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-12 col-sm-12">
				<div class="footer-img">
					<img src="img/fut-logo.png" alt="">
				</div>
			</div>
			<div class="col-md-3 col-6 col-sm-6">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a href="#" class="nav-link"><strong>Компания</strong></a></li>
						<li class="nav-item">
							<a href="#" class="nav-link">Наша кухня</a></li>
							<li class="nav-item">
								<a href="#" class="nav-link">Доставка</a></li>
								<li class="nav-item">
									<a href="#" class="nav-link">Оплата</a></li>
									<li class="nav-item">
										<a href="#" class="nav-link">Контакты</a></li>
									</ul>
								</div>
								<div class="col-md-2 col-6 col-sm-6">
									<ul class="navbar-nav">
										<li class="nav-item">
											<a href="#" class="nav-link"><strong>Помошь</strong></a></li>
											<li class="nav-item">
												<a href="#" class="nav-link">Отзывы</a></li>
												<li class="nav-item">
													<a href="#" class="nav-link">Вакансий</a></li>
													<li class="nav-item">
														<a href="#" class="nav-link">Корпоративным клиентам</a></li>
													</ul>
												</div>
												<div class="col-md-4 col-12 col-sm-12">
													<p>Подпишитесь на рассылку и оставайтесь в курсе событий</p>
													<form action="form" class="header-form">
														<input type="email" placeholder="Ваш E-mail">
														<button class="btn btn-success">Подписаться</button>	
													</form>
													<div class="footer-img">
														<a href="#"><img src="img/logo-visa.png" alt=""></a>
														<a href="#"><img src="img/logo-mastercard.png" alt=""></a>
														<a href="#"><img src="img/apple-logo.png" alt=""></a>
													</div>
												</div>
											</div>
										</div>
									</footer>
									<!--FOOTER  -->	

									<!--END-IMG -->	
									<div class="container-fluid end-img wow bounceInLeft">
										<div class="container">
											<div class="row">
												<div class="col-6 col-sm-6 col-md-3">
													<p>©2019.Doslamarket</p>
												</div>
												<div class="col-6 col-sm-6 col-md-5">
													<div class="end-p">
														<p>Мы в соц.сетях</p>
													</div>
													<div class="end-img-p">
														<a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
														<a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
														<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
													</div>
												</div>
												<div class="col-3 col-sm-12 col-md-4">
													<p>Сайт создан в:<a href="#" class="end-a">A-lux</a></p>
												</div>
											</div>
										</div>
									</div>
									<!--END-IMG -->


									{{-- <script src="https://code.jquery.com/jquery-3.4.1.js"
									integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
									crossorigin="anonymous"></script>
									<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
									<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> --}}
									{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha256-pTxD+DSzIwmwhOqTFN+DB+nHjO4iAsbgfyFq5K5bcE0=" crossorigin="anonymous"></script> --}}
{{-- 									<script src="js/aos.js"></script>
									<script src="js/wow.min.js"></script>
									<script src="js/javaScript.js"></script> --}}
									
									<script src="/js/app.js"></script>
									<script src="/js/all.js"></script>
									<script type="text/javascript">
	$(document).ready(function (){
            refreshTotalSum();
        });
</script>
								</body>
								</html>