<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>@yield('title', env('APP_NAME'))</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="{{ asset('css/aos.css') }}">
	<link rel="stylesheet" href="{{ asset('css/animate.css') }}">
	<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
	<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
  <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
	<script defer type="text/javascript" src="{{ asset('js/all.js') }}"></script>
	<script defer src="{{ asset('js/app.js') }}"></script>
</head>
<body>


	<!-- header -->

	<header>
		<div class="container">
			<div class="header wow fadeInDown">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-3">
						<div class="header-logo">
							<a href="{{ route('main.page') }}"><img src="img/logo.png" alt=""></a>
						</div>
					</div>
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-9">
						<div class="hide-main">
							<div class="row">
								<div class="col-6 col-sm-6 col-md-6 col-lg-3 col-xl-4">
									<div class="job-clock">
										<img src="{{ asset('img/clock.png') }}">
										<h6>График работы</h6>
									</div>
									<div style="display: inline-block;" class="job-clock_1">
										<p>{{ setting('site.schedule') }}</p>
									</div>
								</div>
								<div class="col-6 col-sm-6 col-md-6 col-lg-3 col-xl-2">
									<div class="job-clock">
                                        <img src="{{ asset('img/location.png') }}">
										<h6>Ваш город</h6>
									</div>
									<div style="display: inline-block;" class="job-clock_2">
										<p>Алматы</p>
									</div>
								</div>
								<div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-2">
									<div class="user">
										@auth
										<a href="{{ route('home') }}">Личный Кабинет</a>
										@endauth
										@guest
										<a href="#" data-toggle="modal" data-target="#exampleModalCenter">Войти <span>/</span></a>
										<a href="#" data-toggle="modal" data-target="#exampleModalCenter-login">Зарегистрироваться</a>
										@endguest
									</div>
								</div>
								<div class="col-1 col-sm-1 col-md-2 col-lg-3 col-xl-1 basket">
                                    <a href="{{ route('cart.page') }}"><img src="{{ asset('img/basket.png') }}"></a>
								</div>
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-3 ">
									<button class="btn btn-danger btn-header" data-toggle="modal" data-target="#exampleModalCenter-feedback"><i class="fa fa-phone" aria-hidden="true"></i>Обратная связь</button>
								</div>
							</div>
						</div>
						<div class="main-form">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">
									<div class="main">
										<img src="{{ asset('/img/menu.png') }}" alt="">
										<h6>Menu</h6>
									</div>
								</div>
								<div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10">
									<form action="search" class="header-form">
                                        <input type="text" placeholder="Введите название продукта" name="query" value="{{ \Request::input('query') }}">
										<button class="btn btn-success btn-form"><i class="fa fa-search" aria-hidden="true"></i>Найти</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<div>
		<div class="submenu" id="submenu">
			<div class="container">
				<div><img src="{{ asset('/img/burger-exit.png') }}" id="exit" alt=""></div>
				<div class="row">
                    @foreach (menu('top','_json') as $menuItem)
                    <div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2"><a href="{{ url($menuItem->url) }}">{{ $menuItem->title }}</a></div>
                    @endforeach
				</div>
				<div class="location-mob " style="display: none;">
					<div class="row">
						<div class="col-12">
							<div class="job-clock-mob text-center">
								<img src="img/clock.png">
								<h6>График работы</h6>
							</div>
							<div style="display: inline-block;" class="job-clock_mob-1">
								<p>{{ setting('site.schedule') }}</p>
							</div>
						</div>
						<div class=" col-12">
							<div class="job-clock_mob text-center">
								<img src="img/location.png">
								<h6>Ваш город</h6>
							</div>
							<div style="display: inline-block;" class="job-clock_mob-1">
								<p>Алматы</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="user-mobile">
			<button class="btn btn-danger btn-feedback-mobile" data-toggle="modal" data-target="#exampleModalCenter-feedback"><i><img src="img/phone-header.png" ></i></button>
			<div>
				<a href="#" data-toggle="modal" data-target="#exampleModalCenter"  class="user-mob"><i class="far fa-user-circle"></i></a>
				<a href="basket.html" class="basket-mob"><img src="img/basket.png"></a>
			</div>
		</div>
	</div>

	<!-- МОДАЛЬНОЕ ОКНО -ВОЙТИ -->
	<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Вход</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form name="login-form">
						<div class="modal-text">
							<p>С помощью аккаунта</p>
							<strong><a href="#">Создать аккаунт</a></strong>	
						</div>
						<input type="email" placeholder="Ваш E-mail" name="email">
						<ul id="lemail-errors"></ul>
						<input type="password" placeholder="Пароль" name="password">
						<ul id="lpassword-errors"></ul>
	
						<div class="modal-footer">
							<button type="button" id="login-btn" class="btn btn-success">Войти</button>
							<a href="#">Забыли пароль?</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- МОДАЛЬНОЕ ОКНО -ОБРАТНАЯ СВЯЗЬ -->
	<div class="modal fade" id="exampleModalCenter-feedback" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Обратный звонок</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form name="feedback-form">
						<input type="text" name="phone" placeholder="Ваш Номер">
						<ul id="fphone-errors"></ul>
						<input type="text" name="name" placeholder="Ваше имя">
						<ul id="fname-errors"></ul>
						<div class="modal-footer">
							<button type="button" class="btn btn-success" id="send-feedback">Отправить</button>
						</div>
						<div class="form-group form-check">
							<input type="checkbox" name="agreement" class="form-check-input" id="exampleCheck1">
							<ul id="fagreement-errors"></ul>
							<label class="form-check-label" for="exampleCheck1">Я соглашаюсь с Политикой конфиденциальности обработки персональных данных и даю согласие на обработку своих персональных данных.</label>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- МОДАЛЬНОЕ ОКНО ЗАРЕГЕСТРИРОВАТЬСЯ -->
	<div class="modal fade" id="exampleModalCenter-login" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Регистрация</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="{{ route('register') }}" method="POST" name="register-form">
						@csrf
						<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Ваш Email">
						<ul id="remail-errors"></ul>
						@error('email')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Пароль">
						<ul id="rpassword-errors"></ul>
						@error('password')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Повторите пароль">
						<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Ваше имя">
						<ul id="rname-errors"></ul>
						@error('name')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
						<div class="modal-footer">
							<button type="submit" class="btn btn-success" id="register-btn">Зарегистрироваться</button>
						</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- header -->
@yield('content')
<!--FOOTER  -->
<footer class="container-fluid wow bounceInRight">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-12 col-sm-12">
                <div class="footer-img">
                    <img src="{{asset('/img/fut-logo.png')}}" alt="">
                </div>
            </div>
            @php $bottomMenu = menu('bottom','_json')->split(2) @endphp
            <div class="col-md-3 col-6 col-sm-6">
                <ul class="navbar-nav">
                    @foreach ($bottomMenu[0] as $menuItem)
                    <li class="nav-item">
                        <a href="{{ url($menuItem->url) }}" class="nav-link">{!! $loop->iteration == 1 ? '<strong>' : ''!!}{{ $menuItem->title }}{!! $loop->iteration == 1 ? '</strong>' : '' !!}</a>
                    </li>    
                    @endforeach
                </ul>
            </div>
            <div class="col-md-2 col-6 col-sm-6">
                <ul class="navbar-nav">
                    @foreach ($bottomMenu[1] as $menuItem)
                    <li class="nav-item">
                        <a href="{{ url($menuItem->url) }}" class="nav-link">{!! $loop->iteration == 1 ? '<strong>' : ''!!}{{ $menuItem->title }}{!! $loop->iteration == 1 ? '</strong>' : '' !!}</a>
                    </li>    
                    @endforeach
                </ul>
            </div>
            <div class="col-md-4 col-12 col-sm-12">
                <p>Подпишитесь на рассылку и оставайтесь в курсе событий</p>
                <form action="form" class="header-form">
                    <input type="email" placeholder="Ваш E-mail">
                    <button class="btn btn-success">Подписаться</button>	
                </form>
                <div class="footer-img">
                    <a href="#"><img src="img/logo-visa.png" alt=""></a>
                    <a href="#"><img src="img/logo-mastercard.png" alt=""></a>
                    <a href="#"><img src="img/apple-logo.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--FOOTER  -->	

<!--END-IMG -->	
<div class="container-fluid end-img wow bounceInLeft">
    <div class="container">
        <div class="row">
            <div class="col-6 col-sm-6 col-md-3">
                <p>©2019.Doslamarket</p>
            </div>
            <div class="col-6 col-sm-6 col-md-5">
                <div class="end-p">
                    <p>Мы в соц.сетях</p>
                </div>
                <div class="end-img-p">
                    <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="col-3 col-sm-12 col-md-4">
                <p>Сайт создан в:<a href="https://a-lux.kz" class="end-a">A-lux</a></p>
            </div>
        </div>
    </div>
</div>
<!--END-IMG -->
<script src="https://code.jquery.com/jquery-3.4.1.js"
integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

{{-- <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/aos.js') }}"></script>
<script src="{{ asset('js/wow.min.js') }}"></script>
<script src="{{ asset('js/javaScript.js') }}"></script> --}}
</body>
</html>
