@extends('layouts.app')
@section('title', $title)
@section('content')
<!-- О КОМПАНИЙ -->
<div class="container">
    <div class="company-text wow fadeInUp" style="background-image:url({{ asset('storage/'.$aboutPageContent['О Нас'][0]->image) }})">
        <div class="col-md-8 col-12 col-sm-12">
            <h1 class="display-4">{{ $title }}</h1>
            {!! $aboutPageContent['О Нас'][0]->content !!}
        </div>
    </div>
</div>
<!-- О КОМПАНИЙ -->
@endsection