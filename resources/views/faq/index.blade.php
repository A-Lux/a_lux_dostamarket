@extends('layouts.app')
@section('title', 'Вопросы и ответы')
@section('content')
<!-- Questions -->
<div class="container">
    <div class="questions-main">
        <h1>Частые вопросы</h1>
        <div class="row">
            <div class="col lg-4">
                <div class="questions-left">
                    <ul>
                        @foreach ($faqs as $faq)
                        <li data-tab="qTab-{{ $loop->iteration }}" class="{{ $loop->first ? 'q-tab-active' : '' }}">{{ $faq->question }}</li>  
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-lg-8">
                @foreach ($faqs as $faq)
                <div class="q-tab-content {{ $loop->first ? 'q-tab-content-active' : '' }}" id="qTab-{{ $loop->iteration }}">
                    <br>
                    <p>
                        <b>{{ $faq->question }}</b>
                        <br><br>
                        Регистрация на сайте занимает пару минут:
                    </p>
                    {{ $faq->answer }}
                </div> 
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection