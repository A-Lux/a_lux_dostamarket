@extends('layouts.app')
@section('title', $title)
@section('content')
<!-- DELIVERY-BLOCK -->
<div class="container">
    <div class="delivery-block wow bounceInRight">
        <div class="delivery-title">
            <div class="container">
            <h1>ДОСТАВКА И ОПЛАТА</h1> 	
            <ul class="tabs">
                <div class="row">
                    @foreach ($tabs as $tab)
                    <li class="col-md-2 tab-link text-center {{ $loop->first ? 'current' : '' }}" data-tab="tab-{{ $loop->iteration }}">{{ $tab }}</li>    
                    @endforeach
                </div>
            </ul>
        </div>
    </div>
    <div id="tab-1" class="tab-content current">
        <div class="container">
          {!! $deliveryPageContent['Доставка'][0]->content !!}
        </div>
    </div>
    <div id="tab-2" class="tab-content">
            {!! $deliveryPageContent['Оплата'][0]->content !!}
    </div>
    <div id="tab-3" class="tab-content">
        {!! $deliveryPageContent['Качество'][0]->content !!}
    </div>
    <div id="tab-4" class="tab-content">
        {!! $deliveryPageContent['Возврат товара'][0]->content !!}
    </div>
    </div>
</div>
<!-- DELIVERY-BLOCK -->
@endsection