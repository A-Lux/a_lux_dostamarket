@extends('layouts.app')

@section('content')
<!-- Personal-Area -->
<div class="tabs-block">
    <div class="container">
    <ul class="tabs">
        <li class="col-md-3 tab-link current offset-md-1 text-center" data-tab="tab-1">Личные данные</li>
        <li class="col-md-3 tab-link text-center" data-tab="tab-2">Мои заказы</li>
        <li class="col-md-3 tab-link text-center" data-tab="tab-3">Избанное</li>
    </ul>
    <div id="tab-1" class="tab-content current">
    <form name="user-data">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-4">
                <div class="form-group">
                    <input type="text" name="name" class="form-control" placeholder="Ф.И.О" value="{{ $user->name }}">
                    <ul id="uname-errors"></ul>
                </div>
                <div class="form-group">
                    <input type="text" name="phone" class="form-control" placeholder="Ваш телефон" value="{{ $user->phone }}">
                </div>
                <div class="form-group">
                    <input type="text" name="address" class="form-control" placeholder="Ваш адрес" value="{{ $user->address }}">
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-4 ">
                <div class="form-group">
                    <input type="text" name="additional_address" class="form-control" placeholder="Дополнительный адрес" value="{{ $user->additional_address }}">
                </div>
                <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="Логин" value="{{ $user->email }}">
                    <ul id="uemail-errors"></ul>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-3 offset-md-1">
                <div class="tabs-text">
                    <a href="#" onclick="$('#exampleModal-pasword').modal('show')"><p>Сменить пароль</p></a>
                    <a href="#" id="update-user-data"><p>Редактировать данные</p></a>
                </div>
            </div>
        </div>
    </form>
    </div>
        <div id="tab-2" class="tab-content">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Номер Заказа</th>
                            <th scope="col">Имя товара</th>
                            <th scope="col">Дата оформления</th> 
                            <th scope="col">Статус</th> 
                            <th scope="col">Количество</th>
                            <th scope="col">Цена</th>
                            <th scope="col">Сумма</th>
                            <th scope="col">Итог</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $order)
                            @foreach ($order->products as $product)
                                <tr>
                                    <td scope="col">{{ $order->id }}</td>
                                    <td scope="col">{{ $product->product->title }}</td>
                                    <td scope="col">{{ $order->created_at }}</td> 
                                    <td scope="col">Статус</td> 
                                    <td scope="col">{{ $product->amount }} шт.</td>
                                    <td scope="col">{{ $product->price }} тг.</td>
                                    <td scope="col">{{ $product->amount * $product->price }} тг.</td>
                                    <td scope="col">{{ $loop->last ? $order->total_price.' тг.' : ''}}</td>
                                </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div id="tab-3" class="tab-content">
            <div class="row">
                @foreach ($favoriteProducts as $product)
                <div class="col-sm-12 col-12 col-md-3">
                    <div class="carousel-card-img-2">
                    <div class="carosel-card-tab">
                        <a href="#" class="remove-from-fav"><img prod="{{ $product->id }}" src="{{ asset('img/delete.png') }}" class="like"></a>
                    <div class="card-body text-center">
                        <img src="{{ asset('storage/'.$product->image) }}" alt="{{ $product->title }}">
                    <p class="card-text text-left">{{ $product->title }} {{ $product->short_description }}</p>
                    </div>
                    <div class="card-text-2">
                        <button class="btn btn-success btn-korzina-2"><img src="img/korzina.png" alt="">В корзину</button>
                        <br>
                        <a href="{{ route('product.page', $product->id) }}">Подробнее</a>
                    </div>
                    </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <!-- MОДАЛЬНОЕ ОКНО СМЕНИТЬ ПАРОЛЬ -->
        <div class="modal fade" id="exampleModal-pasword" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Сменить пароль</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form name="change-password">
                            <div class="modal-text">
                                <p></p>
                            </div>
                            <input type="password" name="password_old" placeholder="Старый пароль">
                            <ul id="passwordOldErrors"></ul>
                            <input type="password" name="password" placeholder="Новый пароль">
                            <ul id="passwordNewErrors"></ul>
                            <input type="password" name="password_confirmation" placeholder="Подтвердите новый пароль">
                            <div class="modal-footer">
                                <button id="update-password" type="button" class="btn btn-success">Сохранить</button>
                                <a href="#">Забыли пароль?</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- MОДАЛЬНОЕ ОКНО СМЕНИТЬ ПАРОЛЬ -->
    </div>
</div>
<!-- МОДАЛ КОНТЕНТ -->
<div class="modal fade" id="modal-password-changed" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Пароль был успешно изменен</h5>
            </div>
            <div class="modal-btn">
                <button type="button" class="btn btn-light btn-modal-1" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-info-changed" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Данные были успешно изменены</h5>
            </div>
            <div class="modal-btn">
                <button type="button" class="btn btn-light btn-modal-1" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<!-- МОДАЛ КОНТЕНТ -->
<form method="POST" action="{{ route('logout') }}">
        @csrf
        <button type="submit">Logout</button>
      </form>
<!-- Personal-Area -->
@endsection
