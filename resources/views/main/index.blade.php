@extends('layouts.app')

@section('title', $title)

@section('content')
    <!-- Carousel -->			
	<!-- Carousel -->			
	<div class="carousel-desktop">
		<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active">.</li>
				<li data-target="#carouselExampleIndicators" data-slide-to="1">.</li>
				<li data-target="#carouselExampleIndicators" data-slide-to="2">.</li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3">.</li>
                @for ($i = 4; $i < $sliders->count()+4; $i++)
                <li data-target="#carouselExampleIndicators" data-slide-to="{{ $i }}">.</li>
                @endfor
			</ol>
			<div class="carousel-inner">
				<div class="wp-inst-face">
					<a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
					<br>
					<a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
					<br>
					<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
				</div>
				<div class="carousel-item active">
					<div class="carousel-img-first wow fadeInUp">
						<img src="img/bg-carousel.png" alt="" >
					</div>
					<div class="carousel-caption">
						<h1 class="display-4">DOSTA MARKET</h1>
						<p>Lorem ipsum dolor sit amet, consectetur.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
						<p>Lorem ipsum dolor sit amet, consectetur.</p>
					</div>
					<img src="{{ asset('/img/vishnya.png') }}" alt=""  class="carousel-img-item-1 animat-move 	" >
					<img src="{{ asset('/img/sir.png') }}" alt=""  class="carousel-img-item-2 	animat-move " >
					<img src="{{ asset('/img/brokoli.png') }}" alt=""  class="carousel-img-item-3 	animat-move " >
					<img src="{{ asset('/img/balgarskii.png') }}" alt=""  class="carousel-img-item-4 	animat-move " >
					<img src="{{ asset('/img/tomatto-carousel.png') }}" alt=""  class="carousel-img-item-5 animat-move 	" >
					<img src="{{ asset('/img/moloko.png') }}" alt=""  class="carousel-img-item-6 	animat-move " >
				</div>

				<!-- CAROUSEL-2 -->
				<div class="carousel-item">
					<div class="carousel-2">
						<div class="carousel-img-two">
							<img src="bg-glav.png" alt="" class="bg-glav wow fadeInUp" style="z-index: 99; ">
						</div>
						<div class="carousel-caption-2 text-left wow fadeInLeft">
							<h1 class="display-4">DOSTA MARKET</h1>
							<p>Молочная продукция изготавливается исключительно <br> из натурального высококачественного коровьего молока,<br> без добавления консервантов и ГМО.</p>


							<button class="btn btn-light">Смотреть продукты</button>
						</div>
						<img src="klubnika.png" alt=""  class="animate-item-1 	animat-move">
						<img src="klubnika.png" alt=""  class="animate-item-2 	animat-move">

						<img src="img/vishnya.png" alt=""  class="animate-item-3 animat-move">
						<img src="img/vishnya.png" alt=""  class="animate-item-4 animat-move">

						<img src="chernika.png" alt="" class="animate-item-5 animat-move">
						<img src="chernika.png" alt="" class="animate-item-6 animat-move">

						<img src="kaplya.png" alt="" class="animate-item-9 animat-move">
						<img src="kaplya.png" alt="" class="animate-item-10 animat-move">

						<img src="kaplya2.png" alt="" class="animate-item-7 animat-move">
						<img src="kaplya2.png" alt="" class="animate-item-8 animat-move">
					</div>
				</div>

				<!-- CAROUSEL-3 -->

				<div class="carousel-item">
					<div class="carousel-3">
						<div class="carousel-img-two">
							<img src="img/sl-3-mobile.png" alt="" class="sl-3-mobile wow zoomInUp" style="z-index: 99; ">
						</div>
						<div class="carousel-caption-3">
							<h1 class="display-3">20% </h1>
							<small>кэшбэк</small>
							
							<h1 class="display-4">DOSTA MARKET</h1>
							<p>
								Установи приложение и получай
								<br>
							кэшбэк до 20% в заведениях твоего города</p>
							
						</div>
						<img src="img/tomatto-carousel.png" alt=""  class="tomatto-sl-3 wow zoomInLeft"  data-wow-delay="0.1s">

						<img src="img/cherry.png" alt=""  class="cherry-sl-3 wow zoomInRight"  data-wow-delay="0.3s">
						<img src="img/foliage.png" alt=""  class="foliage-sl-3  wow fadeInLeft zoomInLeft"  data-wow-delay="0.5s">
						<img src="img/straw.png" alt="" class="Strawberry-sl-3 wow zoomInRight" data-wow-delay="0.9s">
						<img src="chernika.png" alt="" class="blueberries-sl-3 wow zoomInLeft" data-wow-delay="0.9s">

						<img src="img/brokoli.png" alt="" class="broccoli-sl-3 wow zoomInRight" data-wow-delay="1s">
						<img src="img/balgarskii.png" alt="" class="balgarski-sl-3  wow zoomInLeft" data-wow-delay="1s">

					</div>
				</div>

				<!-- CAROUSEL-3 -->

				<!-- CAROUSEL-4 -->
				<div class="carousel-item">
					<div class="carousel-4">
						<div class="carousel-img-two">
							
						</div>
						<div class="carousel-caption carousel-text-4">
							<h1 class="display-4">DOSTA MARKET</h1>
							<p>
								Соблюдение качества – это хороший знак.<br> Мы усилили контроль качества.
							</p>
							<button class="btn btn-warning">Смотреть продукты</button>
						</div>
						<img src="img/sl-4-top.png" alt=""  class="sl-4-top wow fadeInDown" >

						<img src="img/sl-4-top-2.png" alt=""  class="sl-4-top-2 wow zoomInDown"  data-wow-delay="0.3s">
						<img src="img/sl-4-top-3.png" alt=""  class="sl-4-top-3  wow zoomInDown"  data-wow-delay="0.5s">
						<img src="img/sl-4-top-3.png" alt=""  class="sl-4-top-3  wow fadeInDown"  data-wow-delay="0.5s">
						<img src="img/sl-4-left.png" alt="" class="sl-4-left wow zoomInRight" data-wow-delay="0.8s">
						<img src="img/sl-4-right.png" alt="" class="sl-4-right wow zoomInLeft" data-wow-delay="0.8s">
						<img src="img/lojka.png" alt="" class="lojka wow zoomInLeft" data-wow-delay="0.8s">
						<img src="img/sl-4-bottom.png" alt="" class="sl-4-bottom wow fadeInUp" data-wow-delay="1s">
						<img src="img/sl-4-bottom-1.png" alt="" class="sl-4-bottom-1  wow zoomInLeft" data-wow-delay="1s">
						<img src="img/sl-4-bottom-2.png" alt="" class="sl-4-bottom-2  wow zoomInLeft" data-wow-delay="1s">

					</div>
				</div>

				<!-- CAROUSEL-4 -->
                @foreach ($sliders as $slider)
                <div class="carousel-item" style="background-image: url({{ asset('storage/'.$slider->image) }})">
                    <div class="carousel-3">
                        <div class="carousel-caption-2 text-left wow fadeInLeft">
                            <h1 class="display-4">{{ $slider->title }}</h1>
                            <p>
                                {!! $slider->text !!}
                            </p>

                            <a class="btn btn-light" href="{{ $slider->link }}">{{ $slider->link_title }}</a>
                        </div>
                    </div>
                </div>
                @endforeach
			</div>
		</div>
	</div>
	<!--/////////////////////// CAROUSEL /////////////////-->

	<!-- /////////////////////////// CAROISEL-MOBILE ///////////////////////////-->
	<div class="carousel-mobile">
		<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			</ol>
			<div class="carousel-inner">
				<div class="wp-inst-face">
					<a href="#"><img src="{{ asset('/img/in-wp-f.png') }}" alt=""></a>
				</div>
				<div class="carousel-item active">
					<img src="{{ asset('/img/main-bg.png') }}" class="d-block w-100" alt="...">
					<div class="carousel-caption">
						<h1 class="display-5">DOSTA MARKET</h1>
						<p>Lorem ipsum dolor sit amet, consectetur.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
						<p>Lorem ipsum dolor sit amet, consectetur.</p>
					</div>
				</div>
				<div class="carousel-item">
					<img src="{{ asset('/img/main-bg.png') }}" class="d-block w-100" alt="...">
					<div class="carousel-caption">
						<h1 class="display-5">DOSTA MARKET</h1>
						<p>Lorem ipsum dolor sit amet, consectetur.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
						<p>Lorem ipsum dolor sit amet, consectetur.</p>
					</div>
				</div>
				<div class="carousel-item">
					<img src="{{ asset('/img/main-bg.png') }}" class="d-block w-100" alt="...">
					<div class="carousel-caption">
						<h1 class="display-5">DOSTA MARKET</h1>
						<p>Lorem ipsum dolor sit amet, consectetur.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
						<p>Lorem ipsum dolor sit amet, consectetur.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--  //////////////////////////CAROISEL-MOBILE ////////////////////////// -->

	<!--  //////////////////////////CATALOG-SLIDER ////////////////////////// -->
	<div class="carousel-product">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="carousel-block">
						<div class="prev">
							<a href="#" class="prev-product"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
						</div>
						<div class="carousel-js">
							<div class="owl-carousel owl-theme catalog-slider">
                                @foreach ($categories as $category)
                                    <div class="slider-item text-center">
                                        <a href="{{ route('catalog.page', $category->id) }}">
                                            <img src="{{ asset('storage/'.$category->image) }}">
                                            <p>{{ $category->name }}</p>
                                        </a>
                                    </div>
                                @endforeach
							</div>
						</div>
						<div class="next">
							<a href="#" class="next-product"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Carousel-Product -->

	<!--Text-IMG-Block-->
	<div class="container">
		<div class="img-block  wow bounceInLeft" style="background-image: url({{ asset('storage/'.$mainPageContent['Почему мы?'][0]->image) }});">
			<div class="col-12 col-sm-12 col-md-5 col-xs-5">
            <h1 class="display-4" style="color: #e54b65;">{{ $mainPageContent['Почему мы?'][0]->block_title }}</h1>
				{!! $mainPageContent['Почему мы?'][0]->content !!}
            </div>
		</div>
	</div>
	<!--Text-IMG-Block-->

	<!--BRAND-Block-->
	<div class="carousel-brand">
		<div class="container carousel-icon">
			<div class="row">
				<div class="col-md-12">
					<div class="carousel-block">
						<div class="prev_">
							<a href="#" class="prev-end"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                        </div>
						<div class="owl-carousel owl-theme catalog-slider-end">
                            @foreach ($partners as $partner)
                                <div class="slider-item text-center">
                                    <img src="{{ asset('storage/'.$partner->image) }}">
                                </div>
                            @endforeach
						</div>
						<div class="next_">
							<a href="#" class="next-end"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div>
		<img src="img/Arrows-up-icon.png" alt="" class="scrollup wow fadeInDown">
	</div>
	<!--BRAND-Block-->	
@endsection