import { Cfg } from "./cfg";

export class Cart {
    constructor() {
        this.path = Cfg.path;
    }
    add(id, q) {
        return axios.post(`${this.path}cart/add`, {id, q});
    }
}