import { Cfg } from './cfg';

export class Favorite {
    constructor() {
        this.path = Cfg.path;
    }
    add(id) {
       return axios.post(`${this.path}favorites/store`, {id});
    }

    remove(id) {
        return axios.post(`${this.path}favorites/${id}`,{id});
    }
}