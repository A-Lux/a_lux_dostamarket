/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


window.Vue = require('vue');
window.Axios = require('axios');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('product', require('./components/product.vue').default);
Vue.component('catalog', require('./components/catalog.vue').default);
Vue.component('subcategory', require('./components/subcategory.vue').default);
Vue.component('catalogCategory', require('./components/catalogCategory.vue').default);

import VuePaginate from 'vue-paginate'
Vue.use(VuePaginate)
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
$(document).ready(function () {
	const app = new Vue({
	    el: '#app',
	});
	$('#search-dropdown').click(function(){
		$('#search-drop').slideToggle(300)('open-menu');
	});

	$('#spa1').click(function(){
		$('#popularity').slideToggle(300)('open-menu');
	});

	$('#rating').click(function(){
		$('#class-rating').slideToggle(300)('open-menu');
	});
})
