import { Cfg } from './cfg';

export class AuthService {
    constructor() {
        this.path = Cfg.path;
    }
    
    register(data) {
        return axios.post(`${this.path}register`, data);
    }

    login(data) {
        return axios.post(`${this.path}login`, data);
    }

    changePassword(data) {
        return axios.post(`${this.path}home/user/password`, data);
    }
}