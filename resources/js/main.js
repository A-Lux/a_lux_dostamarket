import { AuthService } from './class.auth';
import { Favorite } from './class.favorite';
import { renderErrors } from './helpers';
import { Cart } from './class.cart';
import { Feedback } from './class.feedback';
import { User } from './class.user';

// Instances
const auth = new AuthService();
const fav = new Favorite();
const cart = new Cart();
const feedback = new Feedback();
const user = new User();

document.addEventListener('DOMContentLoaded', () => {
    // Fav buttons
    const favBtns = document.getElementsByClassName('add-to-fav');
    const favBtnsRemove = document.getElementsByClassName('remove-from-fav');
    // Auth Buttons
    const registerBtn = document.getElementById("register-btn");
    const loginBtn = document.getElementById("login-btn");
    const changePasswordBtn = document.getElementById("update-password");
    // Add to cart
    const addToCartBtn = document.getElementById('add-to-cart');
    const itemCounter = document.getElementById('item-counter');
    // Feedback
    const feedbackBtn = document.getElementById('send-feedback');
    // Update user info
    const updateUserBtn = document.getElementById('update-user-data');

    // Errors
    const rEmailErrors = document.getElementById('remail-errors');
    const rNameErrors = document.getElementById('rname-errors');
    const rPasswordErrors = document.getElementById('rpassword-errors');

    const lEmailErrors = document.getElementById('lemail-errors');
    const lNameErrors = document.getElementById('lname-errors');
    const lPasswordErrors = document.getElementById('lpassword-errors');

    const fPhoneErrors = document.getElementById('fphone-errors');
    const fNameErrors = document.getElementById('fname-errors');
    const fAgreementErrors = document.getElementById('fagreement-errors');

    const uNameErrors = document.getElementById('uname-errors');
    const uEmailErrors = document.getElementById('uemail-errors');

    const passwordOldErrors = document.getElementById('passwordOldErrors');
    const passwordNewErrors = document.getElementById('passwordNewErrors');

    // Auth events
    registerBtn.addEventListener('click', e => {
        const registerForm = new FormData(document.forms.namedItem('register-form'));
        rEmailErrors.innerHTML = '';
        rNameErrors.innerHTML = '';
        rPasswordErrors.innerHTML = '';

        e.preventDefault();
        auth.register(registerForm)
        .then(function (response) {
            if(response.status == 200) {
                window.location.replace(response.data.redirect);
            }
        })
        .catch(error => {
            const errors = {
                email: {
                    messages: error.response.data.errors.email,
                    block: rEmailErrors
                },
                name: {
                    messages: error.response.data.errors.name,
                    block: rNameErrors
                },
                password: {
                    messages: error.response.data.errors.password,
                    block: rPasswordErrors
                }
            }
            renderErrors(errors);
        });
    });

    loginBtn.addEventListener('click', e => {
        const loginForm = new FormData(document.forms.namedItem('login-form'));

        lEmailErrors.innerHTML = '';
        lPasswordErrors.innerHTML = '';

        e.preventDefault();
        auth.login(loginForm)
        .then(function (response) {
            if(response.status == 200) {
                window.location.replace(response.data.redirect);
            }
        })
        .catch(error => {
            const errors = {
                email: {
                    messages: error.response.data.errors.email,
                    block: lEmailErrors
                },
                password: {
                    messages: error.response.data.errors.password,
                    block: lPasswordErrors
                }
            }

            renderErrors(errors);
        });
    });

    //Fav events
    for (const btn of favBtns) {
        btn.addEventListener('click', e => {
            const id = e.target.getAttribute('prod');
            fav.add(id)
                .then(response => {
                    console.log(response)
                })
                .catch(error => {
                    // Unauthorized
                    console.log(error.response)
                });
        });
    } 

    for (const btn of favBtnsRemove) {
        btn.addEventListener('click', e => {
            const id = e.target.getAttribute('prod');
            fav.remove(id)
                .then(response => {
                    e.target.parentNode.parentNode.parentNode.parentNode.remove();
                })
                .catch(error => {
                    // Unauthorized
                    console.log(error.response)
                });
        });
    }

    // Add product to cart
    if(addToCartBtn) {
        addToCartBtn.addEventListener('click', e => {
            const prodId = e.target.getAttribute('prodid');
            cart.add(prodId, itemCounter.value)
                .then(response => console.log(response))
                .catch(error => console.log(error));
        });
    }

    // Send feedback
    feedbackBtn.addEventListener('click', e => {
        fPhoneErrors.innerHTML = '';
        fNameErrors.innerHTML = '';
        fAgreementErrors.innerHTML = '';

        const data = new FormData(document.forms.namedItem('feedback-form'));

        feedback.send(data)
            .then(response => console.log(response))
            .catch(error => {
                const errors = {
                    phone: {
                        messages: error.response.data.errors.phone,
                        block: fPhoneErrors
                    },
                    name: {
                        messages: error.response.data.errors.name,
                        block: fNameErrors
                    },
                    agreement: {
                        messages: error.response.data.errors.agreement,
                        block: fAgreementErrors
                    }
                }
    
                renderErrors(errors);
            });
    });

    updateUserBtn.addEventListener('click', e => {
        const data = new FormData(document.forms.namedItem('user-data'));

        user.update(data)
            .then(response => {
                $("#modal-info-changed").modal('show');
            })
            .catch(error => {
                const errors = {
                    email: {
                        messages: error.response.data.errors.email,
                        block: uEmailErrors
                    },
                    name: {
                        messages: error.response.data.errors.name,
                        block: uNameErrors
                    }
                }
    
                renderErrors(errors);
            });
    });

    if(changePasswordBtn) {
        changePasswordBtn.addEventListener('click', e => {
            passwordOldErrors.innerHTML = '';
            passwordNewErrors.innerHTML = '';

            const data = new FormData(document.forms.namedItem('change-password'));
            auth.changePassword(data)
                .then(response => {
                    $("#exampleModal-pasword").modal('hide');
                    $("#modal-password-changed").modal('show');
                })
                .catch(error => {
                    const errors = {
                        passwordOld: {
                            messages: error.response.data.errors.password_old,
                            block: passwordOldErrors
                        },
                        passwordNew: {
                            messages: error.response.data.errors.password,
                            block: passwordNewErrors
                        }
                    }
                    renderErrors(errors);
                })
        });
    }
});