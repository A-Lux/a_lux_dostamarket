import { Cfg } from "./cfg";

export class Feedback {
    constructor() {
        this.path = Cfg.path;
    }

    send(data) {
        return axios.post(`${this.path}feedback`, data);
    }
}