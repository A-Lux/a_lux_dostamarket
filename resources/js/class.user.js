import { Cfg } from "./cfg";

export class User {
    constructor() {
        this.path = Cfg.path;
    }

    update(data) {
        return axios.post(`${this.path}home/user/update`, data);
    }
}