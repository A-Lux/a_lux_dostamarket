function renderErrors(errors) {
    for (const error in errors) {
        if (errors.hasOwnProperty(error)) {
            if(errors[error].messages) {
                for (const message of errors[error].messages) {
                    errors[error].block.innerHTML += `<li class="text-danger">${message}</li>`;   
                }
            }
        }
    }
}

export { renderErrors };