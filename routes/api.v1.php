<?php 

$router->get('/', function() {
    return [];
});

// Categories
$router
    ->apiResource('/categories','CategoryController')
    ->only(['index', 'show']);

// Products by category
$router
    ->get('/products/{category}', 'ProductController@index')
    ->name('catalog');

// Product
$router
    ->get('/offer/{product}', 'ProductController@show')
    ->name('product');

// Sliders
$router
    ->get('/sliders', 'SliderController@index')
    ->name('sliders');

// Faqs
$router
    ->get('/faqs', 'FaqController@index')
    ->name('faqs');

// Jobs
$router
    ->get('/jobs', 'JobController@index')
    ->name('jobs');

// Brands
$router
    ->get('/brands', 'BrandController@index')
    ->name('brands');
// Contacts
$router
    ->get('/contacts', 'ContactController@index')
    ->name('contacts');

// About
$router
    ->get('/about', 'AboutController@index')
    ->name('about');

// Delivery
$router
    ->get('/delivery', 'DeliveryController@index')
    ->name('delivery');

// Login and registration
$router
    ->namespace('Auth')
    ->group(function() use ($router) {
        $router->post('/register', 'RegisterController@register');
        $router->post('/login', 'LoginController@login');
    });

$router
    ->middleware(['auth:api'])
    ->namespace('Auth')
    ->group(function() use ($router) {
        // Logout
        $router
            ->post('/logout', 'LoginController@logout')
            ->name('logout');
        
        // Change user info
        $router
            ->prefix('user')
            ->group(function() use ($router) {
                // Orders
                $router
                    ->get('/orders', 'OrderController@index')
                    ->name('user.orders');
                
                // Favorites
                $router
                    ->get('/favorites', 'FavoriteController@index')
                    ->name('user.favorites');
                
                // User info
                $router
                    ->get('/', 'UserController@index')
                    ->name('user');
                
                // Change user info
                $router
                    ->post('/update', 'UserController@update')
                    ->name('user.update');
            });
    });