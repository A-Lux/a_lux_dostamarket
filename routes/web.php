<?php

use App\Cart\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// MainPage
Route::get('/', 'MainController@index')
    ->name('main.page');

//Company Page
Route::get('/company', 'CompanyController@index')
    ->name('company.page');

//Jobs Page
Route::get('/jobs', 'JobController@index')
    ->name('jobs.page');

//Contacts Page
Route::get('/contacts', 'ContactController@index')
    ->name('contacts.page');

// Catalog Page
Route::get('/catalog', 'ProductController@index')
    ->name('catalog.page');

// Search Page
Route::get('/search', 'SearchController@index')
    ->name('search.page');

//Product Page
Route::get('/products/{product}', 'ProductController@show')
    ->name('product.page');

//Delivery Page
Route::get('/delivery', 'DeliveryController@index')
    ->name('delivery.page');

//FAQ Page
Route::get('/faq', 'FaqController@index')
    ->name('faq.page');
//About Page
Route::get('/about', 'AboutController@index')
    ->name('about.page');
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


Auth::routes();

Route::post('/feedback', 'FeedbackController@send')
    ->name('feedback.page');

Route::middleware(['auth'])
    ->group(function() {
        Route::prefix('home')
            ->group(function() {
                Route::get('/', 'HomeController@index')
                    ->name('home');
                Route::post('/user/update', 'HomeController@update')
                    ->name('update.user');
            
                Route::post('/user/password', 'HomeController@password')
                    ->name('update.password');
            });
        Route::post('/favorites/store', 'FavoriteController@store');
        Route::post('/favorites/{id}', 'FavoriteController@destroy');
    });

// Cart
Route::prefix('/cart')
    ->group(function() {
        Route::get('/', 'CartController@index')
            ->name('cart.page');
        
        Route::post('add', 'CartController@add')->name('cart.add');
        Route::post('remove', 'CartController@remove')->name('cart.remove'); 
        Route::post('clear', 'CartController@clear')->name('cart.clear');

        Route::middleware(['auth'])
            ->post('order', 'CartController@order')
            ->name('order');
    });
