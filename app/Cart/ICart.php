<?php 

namespace App\Cart;

interface ICart {
    function add($id, $q);
    function remove($id);
    function clear();
    function count();
    function getItems();
}