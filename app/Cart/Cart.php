<?php

namespace App\Cart;

use App\Product;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class Cart implements ICart {
    private $items = [];

    public function __construct() {
        if(Cookie::has('cart')) {
            $this->items = json_decode(Cookie::get('cart'), true);
        }
    }

    public function add($id, $q) {
        $product = Product::select(['id', 'title', 'price'])
            ->where('id', $id)
            ->first();
        
        $product->amount = $q;
        $this->items[$id] = $product;
        $this->updateState();
    }

    public function remove($id) {
        unset($this->items[$id]);
        $this->updateState();
    }

    public function count() {
        $count = 0;

        foreach($this->items as $item) {
            $count += $item['amount'];
        }

        return $count;
    }

    public function totalPrice() {
        $count = 0;

        foreach($this->items as $item) {
            $count += $item['amount'] * $item['price'];
        }

        return $count;
    }

    public function clear() {
        $this->items = [];
        $this->updateState();
    }

    public function getItems() {
        return $this->items;
    }

    public function updateState() {
        Cookie::queue(Cookie::make('cart', json_encode($this->items), 3600));
    }
}