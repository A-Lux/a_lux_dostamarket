<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductInOrder extends Model
{
    protected $fillable = ['order_id', 'product_id', 'amount', 'price'];

    public function product() {
        return $this->belongsTo('\App\Product');
    }
}
