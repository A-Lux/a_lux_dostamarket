<?php

namespace App\Traits;

use App\Http\Filter\Filter;


trait CanFilteredTrait {

	public function scopeFilter($query, Filter $filter)
	{

		return $filter->apply($query);
	}
}
