<?php

namespace App;

use App\Traits\CanFilteredTrait;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use CanFilteredTrait;
}
