<?php

namespace App\Http\Controllers;

use App\Page;
use App\Slider;
use App\Partner;
use App\Category;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index(Slider $slider, Category $category, Page $page, Partner $partner) {
        
        $sliders = $slider
            ->get();
        
        $categories = $category
            ->whereNull('parent_id')
            ->get();
        
        $mainPage = $page
            ->where('title', 'Главная')
            ->first();
        
        $title = $mainPage->title;
        $mainPageContent = $mainPage
            ->content
            ->groupBy('block_title');
            
        $partners = $partner
            ->get();

        return view('main.index', compact('sliders', 'categories', 'mainPage', 'title', 'mainPageContent', 'partners'));
    }
}
