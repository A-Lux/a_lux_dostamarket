<?php 

namespace App\Http\Controllers;

use App\Order;
use App\Cart\Cart;
use App\ProductInOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;

class CartController extends Controller {
    
    public function index(Cart $cart) {
        return view('cart', ['products' => $cart->getItems()]);
    }

    public function add(Request $request, Cart $cart) {
        $this->validate($request, [
            'id' => 'required|integer',
            'q' => 'required|integer'
        ]);
        
        $cart->add($request->id, $request->q);
        
        return response(['added' => $request->id, 'count' => $cart->count()], 200);
    }

    public function remove(Request $request, Cart $cart) {
        $this->validate($request, [
            'id' => 'required|integer',
        ]);
            
        $cart->remove($request->id);
        return response(['removed' => $request->id, 'count' => $cart->count()], 200);
    }

    public function clear(Request $request, Cart $cart) {
        $cart->clear();
        return response(['cleared' => true, 'count' => $cart->count()], 200);
    }

    public function order(Request $request, Cart $cart) {
        $order = Order::create([
            'user_id' => $request->user()->id,
            'total_price' => $cart->totalPrice()
        ]);
        
        foreach($cart->getItems() as $product) {
            ProductInOrder::create([
                'order_id' => $order->id,
                'product_id' => $product['id'],
                'amount' => $product['amount'],
                'price' => $product['price']
            ]);
        }

        $cart->clear();
        return redirect()->route('main.page')->with('order', $order->id);
    }
}