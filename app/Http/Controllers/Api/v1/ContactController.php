<?php

namespace App\Http\Controllers\Api\v1;

use App\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function index(Contact $contact) {
        $contacts = $contact->first();

        return response($contacts);
    }
}
