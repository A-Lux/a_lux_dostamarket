<?php

namespace App\Http\Controllers\Api\v1;

use App\Product;
use App\Category;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Filter\ProductFilter;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index(Category $category, Product $product, ProductFilter $filter) {
        
        $categories = Category::whereNull('parent_id')
            ->get();
        
        $ids = [];
        $ids[] = $category->id;
        $children = Arr::flatten($category->childrenIds()->toArray());
        $category = Arr::collapse([$ids, $children]);
        
        $products = $product
            ->whereIn('category_id', $category)
            ->get();
        
        return response($products);
    }

    public function show(Product $product) {
        
        return response($product);
    }
}
