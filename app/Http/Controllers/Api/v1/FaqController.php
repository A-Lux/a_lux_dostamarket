<?php

namespace App\Http\Controllers\Api\v1;

use App\Faq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    public function index(Faq $faq) {

        $faqs = $faq
            ->paginate(4);
        
        return response($faqs);
    }
}
