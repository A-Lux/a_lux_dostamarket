<?php

namespace App\Http\Controllers\Api\v1;

use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{
    public function index(Slider $slider) {
        
        $sliders = $slider
            ->get();
        
        return response($sliders);
    }
}
