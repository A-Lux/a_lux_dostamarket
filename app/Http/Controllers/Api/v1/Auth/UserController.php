<?php

namespace App\Http\Controllers\Api\v1\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index(Request $request) {

        return response($request->user());
    }

    public function update(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$request->user()->id
        ]);

        $request->user()
            ->fill([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone ?: '',
                'address' => $request->address ?: '',
                'additional_address' => $request->additional_address ?: '',
            ])->save();
        
        return response(['updated' => $request->user()->id], 200);
    }
}
// OVMkDb1xy8G6iFvkPSn23wzLaZmNg62D0tbBMeVH1CH8pQ3pIgHUgvfm1T5b