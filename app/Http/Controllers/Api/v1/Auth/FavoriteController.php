<?php

namespace App\Http\Controllers\Api\v1\Auth;

use App\Favorite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FavoriteController extends Controller
{
    public function index(Request $request) {
        $favorites = Favorite::with('products')
            ->where('user_id', $request->user()->id)
            ->get();
        
        return response($favorites);
    }

    public function store(Request $request) {
        $this->validate($request, [
            'id' => 'integer|required'
        ]);
        
        $favorite = Favorite::create([
                'user_id' => $request->user()->id,
                'product_id' => $request->id
            ]);
        
        return response(['created' => $favorite->id], 200);
    }

    public function destroy(Request $request, $id) {
        $this->validate($request, [
            'id' => 'integer|required'
        ]);
        Favorite::where('product_id', $request->id)
            ->where('user_id', $request->user()->id)
            ->first()
            ->delete();

        return response(['removed' => $id]);
    }
}
