<?php

namespace App\Http\Controllers\Api\v1\Auth;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function index(Request $request) {
        $orders = Order::with('products.product')
            ->where('user_id', $request->user()->id)
            ->get();

        return response($orders);
    }
}
