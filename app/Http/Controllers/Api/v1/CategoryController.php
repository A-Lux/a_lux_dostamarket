<?php

namespace App\Http\Controllers\Api\v1;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index(Category $category) {
        $categories = $category
            ->with('children')
            ->whereNull('parent_id')
            ->get();

        return response($categories);
    }

    public function show(Category $category) {
        $category->load('children');
        return response($category);
    }
}
