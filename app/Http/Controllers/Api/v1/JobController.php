<?php

namespace App\Http\Controllers\Api\v1;

use App\Job;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobController extends Controller
{
    public function index(Job $job) {
        $jobs = $job
            ->paginate(3);
        
        return response($jobs);
    }
}
