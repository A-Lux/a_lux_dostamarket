<?php

namespace App\Http\Controllers;

use App\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function send(Request $request, Feedback $feedback) {
        $this->validate($request, [
            'phone' => 'required',
            'name' => 'required',
            'agreement' => 'required'
        ]);

        Feedback::create([
            'phone' => $request->phone,
            'name' => $request->name
        ]);

        return response(['feedback' => true], 200);
    }
}
