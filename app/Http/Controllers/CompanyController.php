<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function index(Page $page) {
        $companyPage = $page
            ->where('title', 'О Нас')
            ->first();
        $title = $companyPage->title;
        $companyPageContent = $page->content;

        return response([$companyPage, $title, $companyPageContent]);
    }
}
