<?php

namespace App\Http\Controllers;

use App\Faq;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public function index(Faq $faq) {
        $faqs = $faq->get();

        return view('faq.index', compact('faqs'));
    }
}
