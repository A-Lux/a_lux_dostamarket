<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index(Page $page) {
        
        $aboutPage = $page
            ->where('title', 'О Нас')
            ->first();

        $title = $aboutPage->title;
        $aboutPageContent = $aboutPage
            ->content
            ->groupBy('block_title');
            
        return view('about.index', compact('title', 'aboutPageContent'));
    }
}
