<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request, Product $product) {
        $products = $product
            ->where('title', 'LIKE', '%'.$request->input('query').'%')
            ->get();
        
        return view('searches.index', compact('products'));
    }
}