<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class DeliveryController extends Controller
{
    public function index(Page $page) {
        $deliveryPage = $page
            ->where('title', 'ДОСТАВКА И ОПЛАТА')
            ->first();

        $title = $deliveryPage->title;
        $deliveryPageContent = $deliveryPage
            ->content
            ->groupBy('block_title');
        $tabs = $deliveryPageContent->keys();
        return view('deliveries.index', compact('title', 'deliveryPageContent', 'tabs'));
    }
}
