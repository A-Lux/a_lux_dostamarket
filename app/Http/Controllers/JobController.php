<?php

namespace App\Http\Controllers;

use App\Job;
use Illuminate\Http\Request;

class JobController extends Controller
{
    public function index(Job $job) {
        $jobs = $job
            ->paginate(6);

        return view('jobs.index', compact('jobs')); 
    }
}
