<?php

namespace App\Http\Controllers;

use App\Favorite;
use Illuminate\Http\Request;

class FavoriteController extends Controller
{
    public function store(Request $request) {
        $this->validate($request, [
            'id' => 'integer|required'
        ]);
        
        $favorite = Favorite::create([
                'user_id' => $request->user()->id,
                'product_id' => $request->id
            ]);
        
        return response(['created' => $favorite->id], 200);
    }

    public function destroy(Request $request, $id) {
        $this->validate($request, [
            'id' => 'integer|required'
        ]);
        Favorite::where('product_id', $request->id)
            ->where('user_id', $request->user()->id)
            ->first()
            ->delete();

        return response(['removed' => $id]);
    }
}
