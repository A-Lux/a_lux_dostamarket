<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Filter\ProductFilter;

class ProductController extends Controller
{
    public function index(Category $category, Product $product, ProductFilter $filter) {
                
        return view('catalog');
    }

    public function show(Product $product) {
        
        return view('products.show', compact('product'));
    }
}
