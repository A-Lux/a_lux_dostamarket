<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\Favorite;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request, Favorite $favorite, Product $product)
    {
        $orders = Order::with('products.product')->where('user_id', $request->user()->id)->limit(5)->get();
        
        $user = $request->user();
        $favorites = $favorite
            ->select(['product_id'])
            ->where('user_id', $user->id)
            ->get()
            ->toArray();
        
        $favoriteProducts = $product
            ->whereIn('id', Arr::flatten($favorites))
            ->get();
        
        return view('home', compact('user', 'favoriteProducts', 'orders'));
    }

    public function update(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$request->user()->id
        ]);

        $request->user()
            ->fill([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone ?: '',
                'address' => $request->address ?: '',
                'additional_address' => $request->additional_address ?: '',
            ])->save();

        return response(['updated' => $request->user()->id], 200);
    }

    public function password(Request $request) {
        $this->validate($request, [
            'password_old' => 'required',
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
        
        if(!Hash::check($request->password_old, $request->user()->password)){
            return response(['errors' => ['password_old' => ['The specified password does not match the database password']]], 422);
        }

        $request->user()->fill([
            'password' => Hash::make($request->password)
        ])->save();

        return response(['password_updated' => $request->user()->id]);
    }
}
