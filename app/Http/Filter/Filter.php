<?php

namespace App\Http\Filter;

use Illuminate\Http\Request;

abstract class Filter {

	protected $request;

	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function apply($builder)
	{
		foreach ($this->filters() as $filter => $value) {
			if (method_exists($this, $filter) && $value != null) {
				$builder = $this->$filter($builder, $value);
			}
		}

		return $builder;
	}

	public function filters()
	{
		$filters = $this->request->only($this->filters);
		return array_filter($filters);
	}

}
