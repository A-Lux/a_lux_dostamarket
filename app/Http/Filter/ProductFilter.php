<?php 

namespace App\Http\Filter;

use App\Experience;
use App\Http\Filter\Filter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class ProductFilter extends Filter {

   public $filters = ['brands', 'price_from', 'price_to', 'price_sort', 'popularity_sort', 'novelty_sort', 'title'];

   public function brands($builder, $value) {
      
   }

   public function price_from($builder, $value) {
      
   }

   public function price_to($builder, $value) {

   }

   public function price_sort($builder, $value) {

   }

   public function popularity_sort($builder, $value) {

   }

   public function novelty_sort($builder, $value) {

   }

   public function title($builder, $value) {

   }
}
